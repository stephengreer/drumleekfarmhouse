<footer class="content-info" role="contentinfo">
  <div class="container">
    <?php dynamic_sidebar('sidebar-footer'); ?>
  </div>
  <div class="copyrights">
  	<div class="container">
	  	&copy;<?php echo date("Y"); ?> Drumleek Farmhouse All Rights Reserved. | E: <a href="mailto:drumleekfarmhouse@gmail.com?Subject=Website%20Contact" target="_top">drumleekfarmhouse@gmail.com</a><span class="copyright-float">Site designed by: <a href="http://stephengreer.me/" target="_blank">Stephen Greer</a></span>
  	</div>
  </div>
</footer>
