<?php use Roots\Sage\Nav\NavWalker; ?>

<header class="banner navbar navbar-default navbar-static-top" role="banner">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only"><?= __('Toggle navigation', 'sage'); ?></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?= esc_url(home_url('/')); ?>"><?php bloginfo('name'); ?></a>
    </div>

    <nav class="collapse navbar-collapse" role="navigation">
      <?php
      if (has_nav_menu('primary_navigation')) :
        wp_nav_menu(['theme_location' => 'primary_navigation', 'walker' => new NavWalker(), 'menu_class' => 'nav navbar-nav']);
      endif;
      ?>
    </nav>
  </div>
  <nav class="navbar navbar-sub">
    <div class="container-fluid">
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
              <li><a href="#angling">Angling</a></li>
              <li><a href="#ceramic">Ceramic Studio</a></li>
              <li><a href="#attractions">Local Attractions</a></li>
              <li><a href="#farm">Working Farm</a></li>
              </ul>
        </div>
      </div>
  </nav>
</header>
<div class="header-fill"></div>
<section style="background: url(<?php the_field('intro_background'); ?>) 50% fixed; -webkit-background-size: cover; background-size: cover;" class="intro" data-speed="4" data-type="background">
    <div class="container">
      <?php the_field('intro_content'); ?>
    </div>
</section>