<?php
/**
 * Template Name: Things To Do Template
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
  	<p class="page-subheading">Below are just some of the things you can do on the farm during your stay</p>

  	<div id="sticky-anchor"></div>	 
	<nav class="navbar navbar-sub" id="sticky">
		<div class="container-fluid">
    		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      			<ul class="nav navbar-nav">
        			<li><a href="#angling">Angling</a></li>
			        <li><a href="#ceramic">Ceramic Studio</a></li>
			        <li><a href="#attractions">Local Attractions</a></li>
			        <li><a href="#farm">Working Farm</a></li>
          		</ul>
    		</div>
  		</div>
	</nav>

  <?php get_template_part('templates/content', 'page'); ?>
<?php endwhile; ?>
